import {Component, OnInit} from '@angular/core';
import {ProfileInformation} from './summary.profile';
import {TweenLite} from 'gsap';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./_summary.component.scss']
})
export class SummaryComponent implements OnInit {
  private linkedInProfile = 'https://www.linkedin.com/in/cassianostradolini/';
  private instagramProfile = 'https://www.instagram.com/cassianojaeger/';
  private facebookProfile = 'https://www.facebook.com/cassiano.jaeger';
  private gitlabProfile = 'https://gitlab.com/cassianojaeger';
  private name = 'Cassiano Stradolini';
  private jobTitle = 'Solution Developer';
  private image = 'assets/images/profile.JPG';

  profile: ProfileInformation;

  constructor() {
  }

  ngOnInit(): void {
    this.profile = new ProfileInformation();
    this.profile.name = this.name;
    this.profile.jobTitle = this.jobTitle;
    this.profile.image = this.image;
    this.profile.socialLinks = new Array<object>();

    this.profile.socialLinks.push({url: this.linkedInProfile, icon: 'fa fa-linkedin'});
    this.profile.socialLinks.push({url: this.instagramProfile, icon: 'fa fa-instagram'});
    this.profile.socialLinks.push({url: this.facebookProfile, icon: 'fa fa-facebook-f'});
    this.profile.socialLinks.push({url: this.gitlabProfile, icon: 'fa fa-gitlab'});

    this.animate();
  }

  animate() {
    const words = document.getElementsByName('quote');
    const cite = document.getElementsByTagName('cite');
    const divider = document.getElementsByName('divider')[0];

    const animate = () => {
      let maxDelay = 0;
      let maxDuration = 0;

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < words.length; i++) {
        const word = words[i];

        // @ts-ignore
        const duration = (word.dataset.duration);
        // @ts-ignore
        const delay = (word.dataset.delay);
        // @ts-ignore
        const blur = word.dataset.blur;

        maxDelay = Math.max(Number(delay), maxDelay);
        maxDuration = Math.max(Number(duration), maxDuration);

        // @ts-ignore
        TweenLite.set(word, {
          'filter': `blur(${blur}px)`
        });
        // @ts-ignore
        TweenLite.set(word, {
          className: 'animate',
          transition: `all ${duration}s ease-in ${delay}s`
        });
      }

      // @ts-ignore
      TweenLite.set(cite, {
        className: 'animate',
        transition: `all ${maxDuration}s ease-in ${maxDelay}s`
      });

      // @ts-ignore
      TweenLite.set(divider, {
        className: 'animate',
        transition: `all ${maxDuration}s ease-in ${maxDelay}s`
      });

      // @ts-ignore
      TweenLite.delayedCall((maxDuration + maxDelay), () => {
        const baseDelay = 3;
        // @ts-ignore
        TweenLite.set(words, { className: '', delay: baseDelay });
        // @ts-ignore
        TweenLite.set(cite, { className: '', delay: (baseDelay) });
        // @ts-ignore
        TweenLite.set(divider, { className: '', delay: (baseDelay) });
        // @ts-ignore
        TweenLite.delayedCall((baseDelay + (maxDuration * 2)), animate);
      });
    };

    animate();
  }
}
