export class ProfileInformation {
  name: string;
  jobTitle: string;
  socialLinks: Array<object>;
  image: string;
}
