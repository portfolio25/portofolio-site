import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProjectsViewComponent} from './projects-view/projects-view.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {path: 'projects', component: ProjectsViewComponent, data: {animationState: 'ProjectsPage'}},
  {path: '', component: HomeComponent, data: {animationState: 'HomePage'}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
