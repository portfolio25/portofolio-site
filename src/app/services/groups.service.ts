import { Injectable } from '@angular/core';
import {GenericApiService} from './generic.api.service';
import {Project} from '../models/project';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupsService extends GenericApiService{
  private REST_API_SERVER_ROUTE = '/group';
  private REST_API_SERVER_HOST = 'https://portfolio-site-backend-api.herokuapp.com';
  private REST_API_ENDPOINT =  this.REST_API_SERVER_HOST.concat(this.REST_API_SERVER_ROUTE);

  public getGroupsApi() {
    return this.httpClient
      .get<Array<Project>>(this.REST_API_ENDPOINT)
      .pipe(retry(3), catchError(this.handleError));
  }
}
