import {Injectable} from '@angular/core';
import {catchError, retry} from 'rxjs/operators';
import {Project} from '../models/project';
import {GenericApiService} from './generic.api.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends GenericApiService{
  private REST_API_SERVER_ROUTE = '/project';
  private REST_API_SERVER_HOST = 'https://portfolio-site-backend-api.herokuapp.com';
  private REST_API_ENDPOINT =  this.REST_API_SERVER_HOST.concat(this.REST_API_SERVER_ROUTE);

  public getProjectsApi() {
    return this.httpClient
      .get<Array<Project>>(this.REST_API_ENDPOINT)
      .pipe(retry(3), catchError(this.handleError));
  }
}
