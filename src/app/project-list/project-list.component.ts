import {Component, OnDestroy, OnInit} from '@angular/core';
import {transition, trigger, useAnimation} from '@angular/animations';
import {ProjectsService} from '../services/projects.service';
import {Subject} from 'rxjs';
import {
  fadeIn,
  fadeOut
} from './project-list.animations';
import {Project} from '../models/project';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./_project-list.component.scss'],
  animations: [
    trigger('carouselAnimation', [
      transition('void => *', [useAnimation(fadeIn, {params: { time: '500ms' }} )]),
      transition('* => void', [useAnimation(fadeOut, {params: { time: '500ms' }})]),
    ])
  ]
})
export class ProjectListComponent implements OnInit, OnDestroy {

  public projects: Array<Project>;

  currentSlide = 0;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private projectListService: ProjectsService) {
  }

  ngOnInit(): void {
    this.projectListService.getProjectsApi().subscribe(
      // @ts-ignore
      (data: Array<Project>) => this.projects = data
    );
  }

  onPreviousClick() {
    const nextSlide = this.projects.pop();
    this.projects.unshift(nextSlide);
  }

  onNextClick() {
    const currentSlide = this.projects.shift();
    this.projects.push(currentSlide);
  }

  onThumbnailClick(index) {
    const previousImages = this.projects.splice(0, index);
    this.projects = this.projects.concat(previousImages);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  getLanguage(project: Project) {
    return Object.entries(project.language)[0][0];
  }

  getTags(project: Project) {
    return project.tag_list.map((tag) => '#'.concat(tag)).join(', ');
  }
}
