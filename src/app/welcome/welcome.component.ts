import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: [
    './style/_welcome.component.scss',
    './style/_welcome.border.scss',
    './style/_welcome.next.button.scss'
  ],
})
export class WelcomeComponent implements OnInit {
  curlyBrackets = ' {';
  inverseCurlyBrackets = '}';

  ngOnInit(): void {
    const callback = (entries) =>
      entries.forEach(entry => {
        entry.target.classList.toggle('is-visible');
      });

    const observer = new IntersectionObserver(callback);
    document.querySelectorAll('.show-on-scroll').forEach((target) =>
      observer.observe(target));
  }
}
