export interface Project {
  name: string;
  avatar_url: string;
  web_url: string;
  description: string;
  created_at: string;
  last_activity_at: string;
  language: object;
  tag_list: Array<string>;
  namespace: {
    path: string;
    kind: string;
  };
}
