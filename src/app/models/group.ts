import {Project} from './project';

export interface Group {
  name: string;
  web_url: string;
  description: string;
  created_at: string;
  path: string;
  projects: Array<Project>;
}
