import {Component, OnInit} from '@angular/core';
import {Project} from '../models/project';
import {ProjectsService} from '../services/projects.service';
import {GroupsService} from '../services/groups.service';
import {BehaviorSubject} from 'rxjs';
import {Group} from '../models/group';

@Component({
  selector: 'app-projects-view',
  templateUrl: './projects-view.component.html',
  styleUrls: ['./_projects-view.component.scss']
})
export class ProjectsViewComponent implements OnInit {
  constructor(private projectsService: ProjectsService, private groupsService: GroupsService) {
  }

  public groups: Array<Group>;
  public  projects: Array<Project>;
  public currentProject: Project;
  public isShowingProjectsMenu = true;
  private arrayIndex = 0;

  private error = false;

  ngOnInit(): void {
    this.getProjects();
  }

  public onNextClick() {
    this.arrayIndex += 1;
    this.updateDisplayedProject('next');
  }

  public onPreviousClick() {
    this.arrayIndex -= 1;
    this.updateDisplayedProject('previous');
  }

  public toggleProjectsMenu() {
    const element = this.getElementByClass('projects-menu-overlay');
    element.classList.toggle('hidden');

    this.isShowingProjectsMenu = !this.isShowingProjectsMenu;
  }

  public displaySelectedProjectFromMenu(project: Project, projectIndex: number) {
    this.arrayIndex = projectIndex;
    this.currentProject = project;
    this.toggleProjectsMenu();
  }

  public getLanguages = (project: Project) => Object
    .entries(project.language)
    .map(lang => '#'.concat(lang[0]))
    .join(', ')

  private updateDisplayedProject(action: string) {
    this.animateProjectsScreen(action, this.updateCurrentProject).then(r => r);
  }

  private async animateProjectsScreen(action, updateCurrentProject) {
    const screenContent = this.getElementByClass('screen-content');

    screenContent.classList.add('animate-' + action);

    await this.sleep(250);
    updateCurrentProject.call(this);
    await this.sleep(250);

    screenContent.classList.remove('animate-' + action);
  }

  private updateCurrentProject = () => {
    const projectListSize = this.projects.length;
    this.currentProject = this.projects[(this.arrayIndex % projectListSize + projectListSize) % projectListSize];
  }

  private getGroups() {
    this.groupsService.getGroupsApi().subscribe(
      // @ts-ignore
      (data: Array<Group>) => {
        this.groups = data;
        this.groups.forEach(group => this.projects.push.apply(this.projects, group.projects));
      },
      () => {
        this.error = true;
      },
      () => {
        this.renderData();
      }
    );
  }

  private getProjects() {
    this.projectsService.getProjectsApi().subscribe(
      // @ts-ignore
      (data: Array<Project>) => {
        this.projects = data;
        this.currentProject = this.projects[this.arrayIndex];
      },
      () => {
        this.error = true;
      },
      () => {
        this.getGroups();
      }
    );
  }

  private renderData = () => {
    this.isShowingProjectsMenu = false;
    const element = this.getElementByClass('loading-overlay');
    element.classList.add('hidden');
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private getElementByClass = (className: string) => document.getElementsByClassName(className)[0];
}
